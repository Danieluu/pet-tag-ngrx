import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from '../core/app.component';
import { StoreModule } from '@ngrx/store';
import { petTagReducer } from '../reducer/pet_tag.reducer';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({petTag: petTagReducer})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
