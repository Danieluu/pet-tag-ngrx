import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../pages/home/home.component';
import { CreateComponent } from '../pages/create/create.component';
import { CompleteComponent } from '../pages/complete/complete.component';

const routes: Routes = [
  {
    path:'home',
    loadChildren: '../pages/home/home.module#HomeModule'
  },
  {
    path:'create',
    loadChildren: '../pages/create/create.module#CreateModule'
  },
  {
    path:'complete',
    loadChildren: '../pages/complete/complete.module#CompleteModule'
  },
  {
    path:'',
    redirectTo:'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
