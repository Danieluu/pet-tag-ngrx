import { PetTag } from '../data/pet_tag.model';
import { MockPetTags } from '../data.mork/pet_tag.mock';
import { SELECT_SHAPE, Actions } from '../action/pet_tag.action';
export function petTagReducer(state: PetTag = MockPetTags, action: Actions) {
    switch (action.type) {
        case SELECT_SHAPE:
            return Object.assign({}, state, {
                shape: action.payload
            });
    };
};