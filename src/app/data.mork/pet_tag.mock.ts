import { PetTag } from '../data/pet_tag.model';

export const MockPetTags: PetTag = {
    shape: '',
    font: 'sans-serif',
    text: '',
    clip: false,
    gems: false,
    complete: false
};