import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompleteComponent } from './complete.component';
import { RouterModule } from '@angular/router';


const routes = [
  {path:'',component:CompleteComponent}
]
@NgModule({
  declarations: [CompleteComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class CompleteModule { }
