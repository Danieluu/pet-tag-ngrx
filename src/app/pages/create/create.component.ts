import { Component, OnInit, OnDestroy } from '@angular/core';
import { PetTag } from "../../data/pet_tag.model";
import { Observable, Subscription } from 'rxjs';
import { Store } from "@ngrx/store";


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html'
})
export class CreateComponent implements OnInit, OnDestroy {
  tagState$: Observable<PetTag>;
  private tagStateSubscription: Subscription;
  petTag: PetTag;
  done = false;


  constructor(private store: Store<PetTag>) { 
    this.tagState$ = store.select('petTag');
  }

  ngOnInit() {
    this.tagStateSubscription = this.tagState$.subscribe((state) => {
      this.petTag = state;
      this.done = !!(this.petTag.shape && this.petTag.text);
    });
  }

  ngOnDestroy() {}

}
