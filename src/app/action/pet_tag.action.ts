import { Action } from '@ngrx/store';
import { PetTag } from '../data/pet_tag.model';

export const SELECT_SHAPE =         '[PetTag] SELECT_SHAPE';
export const SELECT_FONT =          '[PetTag] SELECT_FONT';
export const ADD_TEXT =             '[PetTag] ADD_TEXT';
export const TOGGLE_CLIP =          '[PetTag] TOGGLE_CLIP';
export const TOGGLE_GEMS =          '[PetTag] TOGGLE_GEMS';
export const COMPLETE =             '[PetTag] COMPLETE';
export const RESET =                '[PetTag] RESET';

export class Select_Shape implements Action {
    readonly type = SELECT_SHAPE;

    constructor (public payload: string) {}
};

export class Select_Font implements Action {
    readonly type = SELECT_FONT;

    constructor (public payload: string) {}
}



export type Actions =
    Select_Shape |
    Select_Font