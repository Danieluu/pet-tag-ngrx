export interface PetTag{
    shape: string,
    font: string,
    text: string,
    clip: boolean,
    gems: boolean,
    complete: boolean
};